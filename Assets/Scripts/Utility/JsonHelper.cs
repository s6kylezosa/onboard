using System;
using UnityEngine;

// From superpig: https://forum.unity.com/threads/how-to-load-an-array-with-jsonutility.375735/
// Different variation: https://gist.github.com/halzate93/77e2011123b6af2541074e2a9edd5fc0

public static class JsonHelper
{
    public static T[] getJsonArray<T>(string json)
    {
        string newJson = "{ \"array\": " + json + "}";
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
        return wrapper.array;
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] array;
    }
}