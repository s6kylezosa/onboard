using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField] private float m_duration = 5f;
    [SerializeField] private float m_timeElapsed = 0f;

    [SerializeField] private float m_tickInterval = 0f;
    [SerializeField] private int m_tickCount = 0;

    [SerializeField] private bool m_bIsActive = false;
    [SerializeField] private bool m_bIsCompleted = false;
    [SerializeField] private bool m_bUseTicks = false;
    [SerializeField] private bool m_bSelfDestruct = true;

    public float Duration { get => m_duration; }
    public float TimeElapsed { get => m_timeElapsed; }
    public float TickInterval { get => m_tickInterval; }
    public int TickCount { get => m_tickCount; }
    public bool IsActive { get => m_bIsActive; }
    public bool IsCompleted { get => m_bIsCompleted; }
    public bool SelfDestruct { get => m_bSelfDestruct; set => m_bSelfDestruct = value; }

    public delegate void CompletedCallback(float p_duration);
    public delegate void TickCallback(int p_tickCounter, float p_tickInterval);

    public event CompletedCallback OnComplete;
    public event TickCallback OnTick;

    private void Update()
    {
        if (m_bIsActive && !m_bIsCompleted)
        {
            m_timeElapsed += Time.deltaTime;
            if (m_bUseTicks)
            {
                int newTicks = (int)(m_timeElapsed / m_tickInterval);
                while(newTicks > m_tickCount)
                {
                    OnTick(++m_tickCount, m_tickInterval);
                }
            }

            if(m_timeElapsed >= m_duration)
            {
                OnComplete(m_duration);
                if(m_bSelfDestruct) Destroy(gameObject);
            }
        }
    }

    /// <summary>
    /// Creates a new Timer game object.
    /// </summary>
    /// <param name="p_duration">Total duration of the timer.</param>
    /// <param name="p_completeCallback">Listener for OnComplete event.</param>
    /// <param name="p_bStartTimer">If true, immediately starts timer after instantiation.</param>
    /// <param name="p_bSelfDestruct">If true, timer will self destruct once time has elapsed.</param>
    /// <returns>A new Timer game object.</returns>
    public static Timer NewTimer(float p_duration, CompletedCallback p_completeCallback, bool p_bStartTimer = true, bool p_bSelfDestruct = true)
    {
        GameObject newGameObject = new GameObject($"Timer({p_duration})");
        Timer newTimer = newGameObject.AddComponent<Timer>();

        newTimer.m_duration = p_duration;
        newTimer.OnComplete = p_completeCallback;

        if (p_bStartTimer) newTimer.StartTimer();

        return newTimer;
    }

    /// <summary>
    /// Creates a new Timer game object.
    /// </summary>
    /// <param name="p_duration">Total duration of the timer.</param>
    /// <param name="p_completeCallback">Listener for OnComplete event.</param>
    /// <param name="p_tickInterval">Time interval between each tick.</param>
    /// <param name="p_tickCallback">Listener for OnTick event.</param>
    /// <param name="p_bStartTimer">If true, immediately starts timer after instantiation.</param>
    /// <param name="p_bSelfDestruct">If true, timer will self destruct once time has elapsed.</param>
    /// <returns>A new Timer game object.</returns>
    public static Timer NewTimer(float p_duration, CompletedCallback p_completeCallback, float p_tickInterval, TickCallback p_tickCallback, bool p_bStartTimer = true, bool p_bSelfDestruct = true)
    {
        GameObject newGameObject = new GameObject($"Timer({p_duration})");
        Timer newTimer = newGameObject.AddComponent<Timer>();

        newTimer.m_duration = p_duration;
        newTimer.m_tickInterval = p_tickInterval;

        newTimer.m_bUseTicks = p_tickInterval > 0 && p_tickInterval <= p_duration;
        
        if (newTimer.m_bUseTicks)
        {
            newTimer.OnTick = p_tickCallback;
            newTimer.OnComplete = p_completeCallback;
        }
        else {
            Debug.LogWarning($"Timer: The tick event will not be triggered with an interval of {p_tickInterval} and duration of {p_duration}.");
        }

        if (p_bStartTimer) newTimer.StartTimer();

        return newTimer;
    }

    /// <summary>
    /// Starts the timer.
    /// </summary>
    public void StartTimer()
    {
        m_bIsActive = true;
    }

    /// <summary>
    /// Pauses the timer. When the timer is started again, the time elapsed before pausing is retained.
    /// </summary>
    public void PauseTimer()
    {
        m_bIsActive = false;
    }

    /// <summary>
    /// Resets the time elapsed and the tick count of the timer. Initial parameters are retained.
    /// </summary>
    /// <param name="p_bPause">If set to true, Timer is paused on reset</param>
    public void ResetTimer(bool p_bPause = false)
    {
        m_timeElapsed = 0;
        m_tickCount = 0;
        m_bIsCompleted = false;

        if (p_bPause) PauseTimer();
    }

    /// <summary>
    /// Resets the time elapsed and the tick count of the timer. Also unsubscribes all listeners.
    /// </summary>
    /// <param name="p_bPause">If set to true, Timer is paused on reset</param>
    public void HardResetTimer(bool p_bPause = true)
    {
        OnTick = null;
        OnComplete = null;
        ResetTimer(p_bPause);
    }

    /// <summary>
    /// Destroys Timer.
    /// </summary>
    public void CancelTimer()
    {
        Destroy(gameObject);
    }

    /// <summary>
    /// Subscribes listener to the OnTick event
    /// </summary>
    /// <param name="p_tickCallback">Is called every tick interval.</param>
    public void SubscribeOnTickEvent(TickCallback p_tickCallback)
    {
        OnTick += p_tickCallback;
    }

    /// <summary>
    /// Subscribes listener to the OnComplete event
    /// </summary>
    /// <param name="p_completedCallback">Is called on timer completion</param>
    public void SubscribeOnCompleteEvent(CompletedCallback p_completedCallback)
    {
        OnComplete += p_completedCallback;
    }

    /// <summary>
    /// Unsubscribes listener from the OnTick event
    /// </summary>
    /// <param name="p_tickCallback">Listener to unsubscribe</param>
    public void UnsubscribeOnTickEvent(TickCallback p_tickCallback)
    {
        OnTick -= p_tickCallback;
    }

    /// <summary>
    /// Unsubscribes listener from the OnComplete event
    /// </summary>
    /// <param name="p_tickCallback">Listener to unsubscribe</param>
    public void UnsubscribeOnCompleteEvent(CompletedCallback p_completedCallback)
    {
        OnComplete -= p_completedCallback;
    }

    /// <summary>
    /// Unsubscribes all listeners before destruction.
    /// </summary>
    private void OnDestroy()
    {
        OnComplete = null;
        OnTick = null;
    }
}
