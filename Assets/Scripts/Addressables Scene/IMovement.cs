using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovement
{
    public void Move(Vector3 p_direction);
    public void MoveTowards(Vector3 p_position);
    public void Jump();
}
