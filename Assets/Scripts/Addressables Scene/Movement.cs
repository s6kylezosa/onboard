using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour, IMovement
{
    public const float MAX_MOVEMENTSPEED = 100.0f;
    public enum MovementState
    {
        Moving,
        Idle,
        COUNT_MAX
    }

    [SerializeField] private Rigidbody m_rb = null;
    [SerializeField] private Camera m_cam = null;

    [SerializeField] private MovementState m_State;

    [SerializeField] private float m_movementSpeed = 25.0f;
    [SerializeField] private float m_targetPositionThreshold = 0.05f;

    [SerializeField] private float m_jumpSpeed = 10.0f;
    [SerializeField] private float m_jumpCooldown = 0.25f;
    private bool m_bCanJump = true;

    [SerializeField] private float m_rotationSpeed = 60;

    private Vector3 m_targetPosition = Vector3.zero;

    [SerializeField] private float m_targetRotationThreshold = 0.99995f;
    private Quaternion m_targetRotation = Quaternion.identity;
    private bool m_bIsRotating = false;

    // Thinking of input compatible with both PC and Mobile

    void Awake()
    {
        m_targetPosition = transform.position;
        if (m_rb == null) { m_rb = GetComponent<Rigidbody>(); }
        if (m_cam == null) { m_cam = Camera.main; }
    }

    void FixedUpdate()
    {
        if (m_bIsRotating)
        {
            float angleDifference = Quaternion.Angle(transform.rotation, m_targetRotation);
            float timeToRotate = angleDifference / m_rotationSpeed;
            float t = Mathf.Min(1f, Time.deltaTime / timeToRotate);

            transform.rotation = Quaternion.Lerp(transform.rotation, m_targetRotation, t);

            m_bIsRotating = Quaternion.Dot(transform.rotation, m_targetRotation) < m_targetRotationThreshold;
        }

        if(m_State == MovementState.Moving)
        {
            Vector3 targetDirection = m_targetPosition - transform.position;
            targetDirection.y = 0;

            float targetDirSqrMag = targetDirection.sqrMagnitude;
            if (Mathf.Pow(m_targetPositionThreshold, 2) >= targetDirSqrMag)
                m_State = MovementState.Idle;

            targetDirection = targetDirSqrMag > 1 ? targetDirection.normalized : targetDirection;
            m_rb.AddForce(targetDirection * m_movementSpeed * Time.deltaTime, ForceMode.VelocityChange);
        }
    }

    public void Move(Vector3 p_direction)
    {
        MoveTowards(transform.position + p_direction);
    }
    public void MoveTowards(Vector3 p_position)
    {
        m_targetPosition = p_position;
        m_targetPosition.y = transform.position.y;
        m_targetRotation = Quaternion.LookRotation(m_targetPosition - transform.position);

        m_State = MovementState.Moving;
        m_bIsRotating = true;
    }
    public void Jump()
    {
        if (m_bCanJump)
        {
            m_bCanJump = false;
            Timer.NewTimer(m_jumpCooldown, (float p_duration) => m_bCanJump = true);
            m_rb.AddForce(transform.up * m_jumpSpeed, ForceMode.VelocityChange);
        }
    }
}
