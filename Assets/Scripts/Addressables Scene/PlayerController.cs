using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private IMovement m_movement;
    [SerializeField] private float m_raycastLength = 100.0f;

    [SerializeField] private Camera m_cam;
    private void Awake()
    {
        if (m_movement == null) { m_movement = GetComponent<IMovement>(); }
        if (m_cam == null) { m_cam = Camera.main; }
    }

    private void Update()
    {
        if(Input.GetButtonDown("Jump") || (Input.touchCount >= 2 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            Debug.Log("Player Controller: Jumped!");
            m_movement.Jump();
        }
        else if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(m_cam.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, m_raycastLength))
            {
                Debug.Log($"PlayerController: Click raycast hit {hit.transform.name} at {hit.point}");
                m_movement.MoveTowards(hit.point);
            }
            else
            {
                Debug.Log("Player Controller: Click raycast hit nothing.");
            }
        }
    }
}
