using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Networking; // UnityWebRequests
using System.Net.Sockets; // TcpClient && Socket
using System.Net; // Socket class helpers
using System.IO; // StreamReader/Writer

using NativeWebSocket; // https://github.com/endel/NativeWebSocket/ Apache License 2.0

using UnityEngine.AddressableAssets; // Addressable Assets package
using UnityEngine.ResourceManagement.AsyncOperations; // Enable to use explicit type of "AsyncOperationHandle" instead of "var"
using System.Linq;

public class AddressableTests : MonoBehaviour
{
    void Start()
    {
        string ipAddress = Dns.GetHostEntry(Dns.GetHostName())
            .AddressList.First(address =>
                address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            .ToString();

        Debug.Log($"IP ADDRESS: {ipAddress}");
        // Events
        // m_timer = Timer.NewTimer(5f, TimerComplete, 1f, TickCallback);

        // Basic WWW / WebRequest
        // StartCoroutine(FetchDataWWW());
        // StartCoroutine(FetchDataWebRequest());

        // Async / Await, Tasks / Future
        // RunAsyncFetches();

        // Websockets Test
        // RunWebSocketsTest();

        // Addressable Assets
        // m_circleGameObject.

        RunAddressablesTest();
    }

    void Update()
    {
        #if !UNITY_WEBGL || UNITY_EDITOR
            if (m_bPollWebsocketData) {
                m_webSocket?.DispatchMessageQueue();
            }
        #endif
    }

    #region TIMER -- events testing
    private Timer m_timer = null;
    void TickCallback(int p_tickCount, float p_tickInterval)
    {
        Debug.Log($"{m_timer.name}: Tick {p_tickCount}: {(float)p_tickCount * p_tickInterval} seconds has elapsed!");
    }

    void TimerComplete(float p_duration)
    {
        Debug.Log($"{m_timer.name}: Completed after {p_duration} seconds!");
    }
    #endregion

    #region ASYNC testing
    // Async Await Support from Asset Store is needed
    private const string USERS_URL = "https://jsonplaceholder.typicode.com/users";

    [System.Serializable]
    public class UserData
    {
        public int id;
        public string name;
        public string username;
        public string email;
        public AddressData address;
        public string phone;
        public string website;
        public CompanyData company;
    }

    [System.Serializable]
    public class AddressData
    {
        public string street;
        public string suite;
        public string city;
        public string zipcode;
        public GeoData geo;
    }

    [System.Serializable]
    public class GeoData
    {
        public float lat;
        public float lng;
    }

    [System.Serializable]
    public class CompanyData
    {
        public string name;
        public string catchPhrase;
        public string bs;
    }
    IEnumerator FetchDataWWW()
    {
        WWW usersRequest = new WWW(USERS_URL);

        Debug.Log("Basic WWW: Users Request START");
        yield return usersRequest;

        if(!string.IsNullOrEmpty(usersRequest.error))
        {
            Debug.LogError($"WWW Error: {usersRequest.error}");
            yield break;
        }

        // UserData userData = JsonUtility.FromJson<UserData>(usersRequest.text);
        UserData[] userDataArray = JsonHelper.getJsonArray<UserData>(usersRequest.text);

        Debug.Log($"Name: {userDataArray[0].name}," +
            $"Address: {userDataArray[0].address?.street}," +
            $"Company: {userDataArray[0].company?.name}");

        Debug.Log("Basic WWW: Users Request END");
    }
    IEnumerator FetchDataWebRequest()
    {
        UnityWebRequest usersRequest = UnityWebRequest.Get(USERS_URL);

        Debug.Log("Basic UnityWebRequest: Users Request START");
        yield return usersRequest.SendWebRequest();

        if(usersRequest.result == UnityWebRequest.Result.Success)
        {
            UserData[] userDataArray = JsonHelper.getJsonArray<UserData>(usersRequest.downloadHandler.text);

            Debug.Log($"Name: {userDataArray[0].name}," +
                $"Address: {userDataArray[0].address?.street}," +
                $"Company: {userDataArray[0].company?.name}");
        }
        else {
            Debug.LogError(usersRequest.error);
        }

        Debug.Log("Basic UnityWebRequest: Users Request END");
    }
    async Task<UserData[]> AsyncFetchDataWWW()
    {
        Debug.Log("Async WWW: Users Request START");
        WWW usersRequest = await new WWW(USERS_URL);

        if (!string.IsNullOrEmpty(usersRequest.error))
        {
            Debug.LogError($"WWW Error: {usersRequest.error}");
            return null;
        }

        // UserData userData = JsonUtility.FromJson<UserData>(usersRequest.text);
        UserData[] userDataArray = JsonHelper.getJsonArray<UserData>(usersRequest.text);

        Debug.Log($"Name: {userDataArray[0].name}," +
            $"Address: {userDataArray[0].address?.street}," +
            $"Company: {userDataArray[0].company?.name}");

        Debug.Log("Async WWW: Users Request END");
        return userDataArray;
    }
    async Task<UserData[]> AsyncFetchDataUnityWebRequest()
    {
        Debug.Log("Async UnityWebRequest: Users Request START");
        UnityWebRequest usersRequest = UnityWebRequest.Get(USERS_URL);

        await usersRequest.SendWebRequest();

        UserData[] userDataArray = null;
        if (usersRequest.result == UnityWebRequest.Result.Success)
        {
            userDataArray = JsonHelper.getJsonArray<UserData>(usersRequest.downloadHandler.text);

            Debug.Log($"Name: {userDataArray[0].name}," +
                $"Address: {userDataArray[0].address?.street}," +
                $"Company: {userDataArray[0].company?.name}");
        }
        else
        {
            Debug.LogError(usersRequest.error);
        }

        Debug.Log("Async UnityWebRequest: Users Request END");
        return userDataArray;
    }
    async void RunAsyncFetches()
    {
        Task<UserData[]> WWWTask = AsyncFetchDataWWW();
        Task<UserData[]> UnityWebRequestTask = AsyncFetchDataUnityWebRequest();

        await Task.WhenAll(WWWTask, UnityWebRequestTask);

        UserData[] WWWResult = await WWWTask;
        UserData[] UnityWebRequestResult = await UnityWebRequestTask;
    }
    #endregion

    #region Websockets Testing
    // wss://echo.websocket.org -- echo test server from https://www.websocket.org/echo.html
    // private const string ECHO_SERVER_URL = "wss://echo.websocket.org/?encoding=text";
    private const string ECHO_SERVER_URL = "ws://localhost:8080";
    private const int ECHO_SERVER_POST = 8080;

    [SerializeField] private bool m_bPollWebsocketData = true;

    void RunWebSocketsTest()
    {
        // RunTcpClientTest();
        RunNWSTest();
    }

    // TCP CLIENT START
    private TcpClient m_echoListener;
    private StreamReader m_echoStreamReader;
    private StreamWriter m_echoStreamWriter;

    [SerializeField] private bool m_bPollTcpData = true;

    void RunTcpClientTest()
    {
        // Connection..?
        m_echoListener = new TcpClient(ECHO_SERVER_URL, 443);
        m_echoStreamReader = new StreamReader(m_echoListener.GetStream());
        m_echoStreamWriter = new StreamWriter(m_echoListener.GetStream());

        StartCoroutine(ReadTcpClient());

        Timer.NewTimer(30f, (float p_duration) =>
        {
            if (m_echoListener.Connected) {
                m_echoStreamWriter.WriteLine("TCP Client: Finished sending messages.");
            }
        }, 5f, (int p_tickCount, float p_tickInterval) => {
            if (m_echoListener.Connected) {
                m_echoStreamWriter.WriteLine($"TCP Client: Sending message {p_tickCount}.");
            }
        });
    }
    IEnumerator ReadTcpClient()
    {
        // Can be set to false through the Inspector to stop loop
        while (m_bPollTcpData)
        {
            if (m_echoListener.Available > 0)
            {
                Debug.Log(m_echoStreamReader.ReadLine());
            }

            yield return null;
        }

        if (m_echoListener.Connected) {
            m_echoListener.Close();
        }
    }
    // TCP CLIENT END ... too much to study atm, and apparently is not supported by WebGL.. 03/25/2021

    // System.Net.Sockets START ... too much to study atm, and apparently is not supported by WebGL.. 03/25/2021
    private Socket m_socket;
    void RunSocketsTest()
    {
        IPHostEntry hostEntry = Dns.GetHostEntry(ECHO_SERVER_URL);

        foreach (IPAddress address in hostEntry.AddressList)
        {
            IPEndPoint ipe = new IPEndPoint(address, ECHO_SERVER_POST);
            Socket tempSocket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            tempSocket.Connect(ipe);

            if (tempSocket.Connected)
            {
                m_socket = tempSocket;
                break;
            }
            else {
                continue;
            }
        }

    }
    // System.Net.Sockets END

    // NativeWebSocket START

    private WebSocket m_webSocket;

    async void RunNWSTest()
    {
        Debug.Log("NWS: Starting Test... ");
        m_webSocket = new WebSocket(ECHO_SERVER_URL);

        m_webSocket.OnOpen += () => Debug.Log("NWS: Connection open");
        m_webSocket.OnError += (error) => Debug.Log($"NWS: Error {error}");
        m_webSocket.OnClose += (code) => Debug.Log("NWS: Connection closed");
        m_webSocket.OnMessage += (bytes) =>
        {
            Debug.Log($"NWS: Message received => {System.Text.Encoding.UTF8.GetString(bytes)}");
        };

        Timer.NewTimer(60f, (float p_duration) => {
            SendWebSocketMessage("Finished sending messages.");

            if (m_webSocket.State != WebSocketState.Closed)
            {
                Debug.Log("NWS: Closing connection");
                m_webSocket.Close();
            }
        }, 5f, (int p_tickCount, float p_tickInterval) => {
            SendWebSocketMessage($"Sending message {p_tickCount}.");
        });

        await m_webSocket.Connect();
        Debug.Log("NWS: Ending Test... ");
    }

    async void SendWebSocketMessage(string p_message)
    {
        if(m_webSocket.State == WebSocketState.Open)
        {
            Debug.Log($"NWS: Sending message.. {p_message}");
            await m_webSocket.SendText(p_message);
            // await m_webSocket.Receive();
        }
    }

    // NativeWebSocket END
    #endregion

    #region ADDRESSABLES

    [SerializeField] private AssetReference m_debrisAssetRef;
    [SerializeField] private GameObject m_debrisGameObject;
    void RunAddressablesTest()
    {
        EventsBasedAddressableTest();
        StartCoroutine(CoroutineAddressablesTest());
        AsyncAddressablesTest();
    }

    void EventsBasedAddressableTest()
    {
        // typeof AsyncOperationHandle<GameObject>
        var loadHandle = Addressables.LoadAssetAsync<GameObject>(m_debrisAssetRef);

        loadHandle.Completed += (lAsyncOpHandler) => // Parameter is the same as loadAsyncOpHandler
        {
            // Congratulations, your asset has been loaded! BUT WAIT, THERE'S MORE! The asset was just LOADED, no GameObject made yet...
            // typeof AsyncOperationHandle<GameObject>
            var instantiateHandle = m_debrisAssetRef.InstantiateAsync(Vector3.zero, Quaternion.identity);
            // Or this .... Addressables.InstantiateAsync(m_debrisAssetRef, Vector3.zero, Quaternion.identity);

            instantiateHandle.Completed += (instAsyncOpHandler) =>
            {
                Debug.Log("E-based Addressable: InstantiateAsync completed and is valid.");
                // Now you should have an instance of your prefab through instAsyncOpHandler.Result

                m_debrisGameObject = instAsyncOpHandler.Result;

                // When this GameObject is destroyed, Addressable.ReleaseInstance(yourGameObject) must be called
                // Jason Weimann - https://www.youtube.com/watch?v=uNpBS0LPhaU

                Timer.NewTimer(3f, (float p_duration) =>
                {
                    Debug.Log("E-based Addressable: Instantiated Game Object destroyed.");
                    Addressables.ReleaseInstance(m_debrisGameObject);
                }, 1f, (int p_tickCount, float p_tickInterval) =>
                {
                    Debug.Log($"E-based Addressable: {p_tickCount}..");
                });
            };
        };
    }

    [SerializeField] private AssetReference m_diamondRef;
    [SerializeField] private Sprite m_diamond;
    [SerializeField] private GameObject m_diamondGameObject;

    IEnumerator CoroutineAddressablesTest()
    {
        Debug.Log("Co Addressable: Loading..");
        var loadHandle = Addressables.LoadAssetAsync<Sprite>(m_diamondRef);
        yield return loadHandle;

        if(loadHandle.Status == AsyncOperationStatus.Succeeded)
        {
            Debug.Log("Co Addressable: Load successful!");
            m_diamond = loadHandle.Result;

            m_diamondGameObject = new GameObject("Diamond", typeof(SpriteRenderer));
            m_diamondGameObject.GetComponent<SpriteRenderer>().sprite = m_diamond;
            m_diamondGameObject.transform.position = Vector3.one * 2;
            Debug.Log("Co Addressable: Game Object instantiated!");
        }
        else
        {
            Debug.Log("Co Addressable: Something went wrong.");
        }
    }

    [SerializeField] private AssetReference m_circleRef;
    [SerializeField] private Sprite m_circle;
    [SerializeField] private GameObject m_circleGameObject;

    async void AsyncAddressablesTest()
    {
        Debug.Log("Async Addressable: Loading..");
        var loadHandle = Addressables.LoadAssetAsync<Sprite>(m_circleRef);
        await loadHandle.Task;

        // NOTE: The AsyncOperationHandle.Task property is not available on WebGL as multi-threaded operations are not supported on that platform.
        // Source: https://docs.unity3d.com/Packages/com.unity.addressables@1.9/manual/AddressableAssetsAsyncOperationHandle.html

        if (loadHandle.Status == AsyncOperationStatus.Succeeded)
        {
            Debug.Log("Async Addressable: Load successful!");
            m_circle = loadHandle.Result;

            m_circleGameObject = new GameObject("Diamond", typeof(SpriteRenderer));
            m_circleGameObject.GetComponent<SpriteRenderer>().sprite = m_circle;
            m_circleGameObject.transform.position = Vector3.one * 4;
            Debug.Log("Async Addressable: Game Object instantiated!");
        }
        else
        {
            Debug.Log("Async Addressable: Something went wrong.");
        }
    }

    #endregion
}
