using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMovement : MonoBehaviour
{
    // Basic movement for initial testing

    public const float MAX_MOVEMENTSPEED = 100.0f;

    [SerializeField] private Rigidbody m_rb = null;
    [SerializeField] private Camera m_cam = null;
    [SerializeField] private float m_movementSpeed = 25.0f;
    [SerializeField] private float m_jumpSpeed = 10.0f;

    private Vector2 m_inputDirection = Vector2.zero;

    void Start()
    {
        if(m_rb == null) m_rb = GetComponent<Rigidbody>();
        if(m_cam == null) m_cam = Camera.main;
    }

    void Update()
    {
        m_inputDirection.x = Input.GetAxisRaw("Horizontal");
        m_inputDirection.y = Input.GetAxisRaw("Vertical");

        if (Input.GetButtonDown("Jump")) m_rb.AddForce(transform.up * m_jumpSpeed, ForceMode.VelocityChange);
    }

    private void FixedUpdate()
    {
        if(m_inputDirection != Vector2.zero)
        {
            Vector3 targetDirection = m_cam.transform.forward * m_inputDirection.y + m_cam.transform.right * m_inputDirection.x;
            targetDirection.y = 0f;
            targetDirection.Normalize();

            m_rb.AddForce(targetDirection * m_movementSpeed * Time.deltaTime, ForceMode.VelocityChange);
        }

        if (m_rb.velocity.y < 0) {
            m_rb.AddForce(Physics.gravity * m_rb.mass);
        }
    }
}
