using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Skill")]
[System.Serializable]
public class Skill : ScriptableObject
{
    [SerializeField] private Sprite m_sprite;
    [SerializeField] private string m_name;
    [SerializeField] private string m_description;
    [SerializeField] private float m_manaCost;
    [SerializeField] private float m_multiplier;
    [SerializeField] private GameObject m_prefab;

    public Sprite Sprite { get => m_sprite; }
    public string Name { get => m_name; set => m_name = value; }
    public string Description { get => m_description; set => m_description = value; }
    public float ManaCost { get => m_manaCost; set => m_manaCost = value; }
    public GameObject Prefab { get => m_prefab; }

    public virtual void Activate(Unit p_caster, Unit p_target)
    {
        Debug.Log($"{m_name} was activated by {p_caster.Name}");
        p_target.Damage(p_caster.AttackDamage * m_multiplier);
    }
}
