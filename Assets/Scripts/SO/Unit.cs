using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Unit")]
public class Unit : ScriptableObject
{
    [SerializeField] private Sprite m_sprite;
    [SerializeField] private string m_name;
    [SerializeField] private string m_description;

    [SerializeField] private int m_level;
    [SerializeField] private float m_currentHealth;
    [SerializeField] private float m_currentMana;

    [SerializeField] private float m_healthRegen;
    [SerializeField] private float m_manaRegen;

    [SerializeField] private Stats m_startStats;
    [SerializeField] private Stats m_statsPerLevel;

    [SerializeField] private List<Skill> m_skills = new List<Skill>(3);

    public Sprite Sprite { get => m_sprite; }
    public string Name { get => m_name; set => m_name = value; }
    public string Description { get => m_description; set => m_description = value; }

    public int Level { get => m_level; set => m_level = value; }
    public float CurrentHealth { get => m_currentHealth; set => m_currentHealth = value; }
    public float CurrentMana { get => m_currentMana; set => m_currentMana = value; }
    public float MaxHealth { get => m_startStats.MaxHealth + Level * m_statsPerLevel.MaxHealth; }
    public float MaxMana { get => m_startStats.MaxMana + Level * m_statsPerLevel.MaxMana; }

    public float HealthRegen { get => m_healthRegen; set => m_healthRegen = value; }
    public float ManaRegen { get => m_manaRegen; set => m_manaRegen = value; }

    public Stats BaseStats { get => m_startStats + Level * m_statsPerLevel; }

    public float AttackDamage { get => BaseStats.Strength; }
    public List<Skill> Skills { get => m_skills; }

    public delegate void UnitEvent(Unit p_unit);

    public void Damage(float p_damage)
    {
        m_currentHealth -= p_damage;
    }

    public void Regen(float p_timeDelta)
    {
        m_currentHealth += m_healthRegen * p_timeDelta;
        m_currentHealth = m_currentHealth < MaxHealth ? m_currentHealth : MaxHealth;

        m_currentMana += m_healthRegen * p_timeDelta;
        m_currentMana = m_currentMana < MaxMana ? m_currentMana : MaxMana;
    }
    
    public bool CanCast(Skill p_skill) { return m_currentMana >= p_skill.ManaCost; }
}