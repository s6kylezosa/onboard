using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Stats
{
    public float MaxHealth;
    public float MaxMana;
    public float Strength;
    public float Defense;

    public float CritRate;
    public float CritDamage;

    public static Stats operator +(Stats lhs, Stats rhs)
    {
        lhs.MaxHealth += rhs.MaxHealth;
        lhs.Strength += rhs.Strength;
        lhs.Defense += rhs.Defense;
        lhs.CritRate += rhs.CritRate;
        lhs.CritDamage += rhs.CritDamage;

        return lhs;
    }

    public static Stats operator *(Stats lhs, Stats rhs)
    {
        lhs.MaxHealth *= rhs.MaxHealth;
        lhs.Strength *= rhs.Strength;
        lhs.Defense *= rhs.Defense;
        lhs.CritRate *= rhs.CritRate;
        lhs.CritDamage *= rhs.CritDamage;

        return lhs;
    }

    public static Stats operator *(int lhs, Stats rhs)
    {
        rhs.MaxHealth *= lhs;
        rhs.Strength *= lhs;
        rhs.Defense *= lhs;
        rhs.CritRate *= lhs;
        rhs.CritDamage *= lhs;

        return rhs;
    }
}