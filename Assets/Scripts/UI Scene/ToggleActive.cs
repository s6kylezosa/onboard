using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleActive : MonoBehaviour
{
    public void Toggle()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    public void ToggleWithRedraw()
    {
        gameObject.SetActive(!gameObject.activeSelf);
        LayoutRebuilder.ForceRebuildLayoutImmediate(gameObject.GetComponent<RectTransform>());
        // May be replaced when tween animation is setup. (Slides from below header and back)
    }
}
