using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemAttractor : MonoBehaviour
{
    [SerializeField] private ParticleSystem m_particleSystem;
    [SerializeField] private Transform m_targetTransform;
    [SerializeField] private float m_force = 10f;
    private ParticleSystem.Particle[] m_particles;

    private void Awake()
    {
        if (m_particleSystem == null) GetComponent<ParticleSystem>();
        if (m_targetTransform == null) m_targetTransform = transform;
    }

    // Think of a solution so that there is no need for polling through Update() i.e. Event-based trigger
    private void Update()
    {
        m_particles = new ParticleSystem.Particle[m_particleSystem.particleCount];
        m_particleSystem.GetParticles(m_particles);

        for(int i = 0; i < m_particles.Length; i++)
        {
            Vector3 direction = m_targetTransform.position - m_particles[i].position;
            Vector3 force = direction.normalized * m_force;
            m_particles[i].velocity = force;
        }

        m_particleSystem.SetParticles(m_particles, m_particles.Length);
    }
}
